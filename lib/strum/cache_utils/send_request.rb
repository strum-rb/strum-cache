# frozen_string_literal: true

require "faraday"
require "faraday_middleware"

module Strum
  module CacheUtils
    # Send request to entity source
    class SendRequest
      include Strum::Service

      private

        TIMEOUT = ENV.fetch("STRUM_CACHE_TIMEOUT", 50).to_i.freeze
        OPEN_TIMEOUT = ENV.fetch("STRUM_CACHE_OPEN_TIMEOUT", 5).to_i.freeze

        def audit
          required(:url)
          sliced(:url, :params)
        end

        def call
          output(send_request.body)
        rescue Faraday::ConnectionFailed => e
          connection_error_handler(e)
        rescue Faraday::TimeoutError => e
          timeout_error_handler(e)
        rescue Faraday::ParsingError => e
          parsing_error_handler(e)
        rescue Faraday::Error => e
          error_handler(e)
        end

        def send_request
          create_faraday_inst.get do |req|
            req.headers["Accept"] = "application/json"
            req.headers["Content-Type"] = "application/json"
            req.params = params if input[:params]
            cache_headers.each { |k, v| req.headers[k] = v }
          end
        end

        def create_faraday_inst
          Faraday.new(url: url) do |f|
            f.response :json
            f.options.timeout = TIMEOUT
            f.options.open_timeout = OPEN_TIMEOUT
            f.use Faraday::Response::RaiseError
          end
        end

        def cache_headers
          Strum::Cache.config.cache_headers.is_a?(Hash) ? Strum::Cache.config.cache_headers : {}
        end

        def connection_error_handler(error)
          add_error(:send_request, :connection_error)
          add_error(:connection_error, [{ info: { error_message: error.message, url: url, method: :get } }])
        end

        def timeout_error_handler(error)
          add_error(:send_request, :timeout_error)
          add_error(:timeout_error, [{ info: { error_message: error.message, url: url, method: :get } }])
        end

        def parsing_error_handler(error) # rubocop: disable Metrics/AbcSize, Style/CommentedKeyword
          info = {
            error_message: error.message,
            request_headers: error.response.env.request_headers,
            response_status: error.response.env.status,
            response_headers: error.response.env.response_headers,
            response_body: error.response.env.response_body
          }
          add_error(:payload, error)
          add_error(:send_request, :json_parse_error)
          add_error(:json_parse_error, [build_format_info(info)])
        end

        def error_handler(error)
          info = {
            error_message: error.message,
            request_headers: error.response[:request][:headers],
            response_status: error.response[:status],
            response_headers: error.response[:headers],
            response_body: error.response[:body]
          }

          add_error(:connection, error)
          add_error(:send_request, :http_error)
          add_error(:http_error, [build_format_info(info)])
        end

        def build_format_info(info)
          {
            info: {
              error_message: info[:error_message], method: :get, url: url,
              request: { headers: info[:request_headers] },
              response: { status: info[:response_status], headers: info[:response_headers], body: info[:response_body] }
            }
          }
        end
    end
  end
end
