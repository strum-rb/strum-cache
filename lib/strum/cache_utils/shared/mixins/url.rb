# frozen_string_literal: true

module Strum
  module CacheUtils
    module Shared
      module Mixins
        # General methods for url builders
        module Url
          def base_url
            ENV.fetch("#{underscore_resource_code.upcase}_#{self.class::RESOURCE_SUFIX_URL}") do
              host = ENV.fetch(self.class::HOST)
              File.join(host, inflector.pluralize(underscore_resource_code.gsub(/_/, "-")))
            end
          end

          def underscore_resource_code
            @underscore_resource_code ||= inflector.underscore(resource_code)
          end

          def inflector
            @inflector ||= Dry::Inflector.new
          end
        end
      end
    end
  end
end
